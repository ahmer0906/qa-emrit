package com.test.emrit.common;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;

public class ConfigReader {
    /**
     * This class is used to read configurations
     **/
    public static final String CONFIG_FILE_PATH = "src/test/resources/configurations/configurations.json";
    public static final String URL = getConfigReader().get("url").asText();

    private ConfigReader() {
    }

    public static JsonNode getConfigReader() {
        return JSONParser.getJsonNode(CONFIG_FILE_PATH).get(getEnv());
    }

    public static String getEnv() {
        if (System.getProperty("env") == null) {
            return "test";
        } else {
            return System.getProperty("env");
        }
    }

    public static String getBrowser() {
        return StringUtils.isEmpty(System.getProperty("browser")) ? "chrome" : System.getProperty("browser");
    }
}
