package com.test.emrit.frontend.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import static com.test.emrit.frontend.tests.SignUpTest.*;

public class SignUpPage extends BasePage {

    @FindBy(xpath = "//input[@name='firstName']")
    private WebElement textFirstName;

    @FindBy(xpath = "//input[@name='lastName']")
    private WebElement textLastName;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement textEmail;

    @FindBy(xpath = "//input[@name='phone']")
    private WebElement phoneNo;

    @FindBy(xpath = "//*[@id='6']/div")
    private WebElement countryMenu;

    @FindBy(xpath = "//div[@class = 'select__menu-list css-11unzgr']/div[contains(text(),'Pakistan')]")
    private WebElement selectMenu;

    @FindBy(xpath = "//input[@name='address-search']")
    private WebElement enterAddress;

    @FindBy(xpath = "//div[contains(text(),'434 Sawan Road, G-10/4 G 10/4 G-10, Islamabad, Pakistan')]")
    private WebElement selectAddress;

    @FindBy(xpath = "//input[@name='age']")
    private WebElement ageCheck;

    @FindBy(xpath = "//input[@name = 'agreePolicies']")
    private WebElement termsAndCondition;

    @FindBy(xpath = "//button[contains(text(),'Sign up')]")
    private WebElement buttonSignUp;

    @FindBy(xpath = "//span[contains(text(),'Wait before next code resend')]")
    private WebElement emailErrorMessage;


    public SignUpPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void fillSignUpPage(String email){
        enterEmail(email);
        enterFirstName(FIRST_NAME);
        enterLastName(LAST_NAME);
        enterPhone(PHONE_NO);
        selectCountry();
        enterAddress();
        agreeTermsAndCondition();
        clickButtonSignUp();

    }

    public void enterFirstName(String firstName){
        type(textFirstName,firstName);
    }

    public void enterLastName(String lastName){
        type(textLastName,lastName);
    }

    public void enterEmail(String email){
        waitUntilElementIsVisible(countryMenu);
        textEmail.sendKeys(email);
    }

    public void enterPhone(String textPhoneNo){
        type(phoneNo,textPhoneNo);
    }

    public void selectCountry(){
        countryMenu.click();
        selectMenu.click();
    }

    public void enterAddress(){
        enterAddress.sendKeys("434 Sawan Road, G-10/4 G 10/4 G-10, Islamabad, Pakistan");
        waitUntilElementIsVisible(selectAddress);
        selectAddress.click();
    }

    public void agreeTermsAndCondition(){
        actionClick(termsAndCondition);
        addWait(1000);
        actionClick(ageCheck);
    }

    public void clickButtonSignUp(){
        buttonSignUp.click();
    }

    public String getEmailMessage(){
        waitUntilElementIsVisible(emailErrorMessage);
        return emailErrorMessage.getText();
    }
}
