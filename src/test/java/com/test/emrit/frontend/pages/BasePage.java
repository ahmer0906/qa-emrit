package com.test.emrit.frontend.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    /**
     * Base Page stores all the common functionalities that is being used by all the child classes
     **/

    protected WebDriver webDriver;
    protected WebDriverWait webDriverWait;
    protected Actions actions;

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.webDriverWait = new WebDriverWait(this.webDriver, 20, 20);
        this.actions = new Actions(this.webDriver);
    }

    protected void waitUntilElementIsVisible(WebElement element) {
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitUntilElementIsClickable(WebElement element) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void actionClick(WebElement element) {
        actions.click(element).perform();
    }

    protected void type(WebElement element, String text) {
        if (!element.getAttribute("value").isEmpty()) {
            element.clear();
        }
        element.sendKeys(text);
    }

    protected void addWait(int waitInMilli) {
        try {
            Thread.sleep(waitInMilli);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
