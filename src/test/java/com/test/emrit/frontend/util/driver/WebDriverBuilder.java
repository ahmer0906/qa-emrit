package com.test.emrit.frontend.util.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverBuilder {
    /**
     * web driver factory class
     **/

    public WebDriver buildWebDriver(String browserName) {
        switch (browserName.toUpperCase()) {
            case "CHROME":
                return getChromeDriver();
            default:
                throw new WebDriverException();
        }
    }

    private WebDriver getChromeDriver() {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

}
