package com.test.emrit.frontend.util.driver;

import com.test.emrit.common.ConfigReader;
import org.openqa.selenium.WebDriver;


public class WebDriverManager {

    /**
     * This class manages all the driver related functionalities
     **/

    private WebDriver driver;

    public WebDriver getWebDriver() {
        if (null == driver) {
            driver = createDriver();
        }
        return driver;
    }

    public void closeDriver() {
        if (null != driver) {
            driver.quit();
            driver = null;
        }
    }

    private WebDriver createDriver() {
        driver = new WebDriverBuilder().buildWebDriver(ConfigReader.getBrowser());
        driver.manage().window().maximize();
        return driver;
    }
}
