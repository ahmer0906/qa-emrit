package com.test.emrit.frontend.tests;

import com.test.emrit.common.ConfigReader;
import com.test.emrit.frontend.pages.EmailConfirmPage;
import com.test.emrit.frontend.pages.MainPage;
import com.test.emrit.frontend.pages.SignUpPage;
import com.test.emrit.frontend.util.helpers.EmailGenerator;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;


public class SignUpTest extends BaseTest {

    public static final String FAILED_SIGN_UP_MESSAGE = "Sign up is not successful";
    public static final String SAME_EMAIL_GENERATE = EmailGenerator.getEmail();
    public static final String EXPECTED_SUCCESS_MESSAGE = "Validate your email";
    public static final String FIRST_NAME = "Test First Name";
    public static final String LAST_NAME = "Test Last Name";
    public static final String PHONE_NO = "12345678901";
    public static final String COUNTRY_NAME = "PAKISTAN";
    public static final String ADDRESS = "434 Sawan Road, G-10/4 G 10/4 G-10, Islamabad, Pakistan";

    @Test(priority = 1, description = "Validate that the user is successfully signing up")
    public void testSignUpSuccessful() {

        // Enter to sign Up page
        MainPage mainPage = new MainPage(webDriver.getWebDriver());
        mainPage.clickMenu();
        mainPage.selectMenu();

        // Fill Sign Up Info
        SignUpPage signUpPage = new SignUpPage(webDriver.getWebDriver());
        signUpPage.fillSignUpPage(EmailGenerator.getEmail());

        // Validate email confirmation message
        String expectedMessage = EXPECTED_SUCCESS_MESSAGE;
        EmailConfirmPage emailConfirmPage = new EmailConfirmPage(webDriver.getWebDriver());
        Assertions.assertThat(
                emailConfirmPage.getTitleText()).as(FAILED_SIGN_UP_MESSAGE).isEqualTo(expectedMessage);
    }

    @Test(priority = 2, description = "Validate error message if user is signing in with the same email twice")
    public void testSignUpWithSameEmailTwice() {

        // Enter to sign Up page
        MainPage mainPage = new MainPage(webDriver.getWebDriver());
        mainPage.clickMenu();
        mainPage.selectMenu();

        // Fill Sign Up Info
        SignUpPage signUpPage = new SignUpPage(webDriver.getWebDriver());
        signUpPage.fillSignUpPage(SAME_EMAIL_GENERATE);

        // Validate email confirmation message
        String expectedMessage = EXPECTED_SUCCESS_MESSAGE;
        EmailConfirmPage emailConfirmPage = new EmailConfirmPage(webDriver.getWebDriver());
        Assertions.assertThat(
                emailConfirmPage.getTitleText()).as(FAILED_SIGN_UP_MESSAGE).isEqualTo(expectedMessage);

        // Submitting sign up information with same email twice
        webDriver.getWebDriver().get(ConfigReader.URL);
        MainPage mainPageTwice = new MainPage(webDriver.getWebDriver());
        mainPageTwice.clickMenu();
        mainPageTwice.selectMenu();

        // Fill Sign Up Info
        SignUpPage signUpPageTwice = new SignUpPage(webDriver.getWebDriver());
        signUpPageTwice.fillSignUpPage(SAME_EMAIL_GENERATE);

        // Validate email confirmation message
        String expectedMessageTwice = "Wait before next code resend";
        Assertions.assertThat(
                        signUpPageTwice.getEmailMessage()).as("Email error message is not shown")
                .isEqualTo(expectedMessageTwice);
    }
}